package com.example.dozencalc

import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

import kotlinx.android.synthetic.main.activity_helpscreen.*
import android.text.method.ScrollingMovementMethod
import android.widget.Button
import androidx.core.text.HtmlCompat

class Helpscreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.content_helpscreen)
        setSupportActionBar(toolbar)

        val scrolly = findViewById<TextView>(R.id.dozencalc_help)
        scrolly.setMovementMethod(ScrollingMovementMethod())
        val sp = HtmlCompat.fromHtml(getString(R.string.dozencalc_help),HtmlCompat.FROM_HTML_MODE_LEGACY)
        scrolly.setText(sp)

        val close = findViewById<Button>(R.id.close)
        val back = Intent(this,MainActivity::class.java)
        close.setOnClickListener {startActivity(back)}
    }

}
