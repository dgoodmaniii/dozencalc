package com.example.dozencalc;

public class baseconv {
    public static double decimalize(final String str) {
        String copy = str;
        int isneg = 0;
        String[] parts;
        int len;
        double fracpart;
        if (copy.charAt(0) == '-') {
            isneg = 1;
            copy = copy.replace("-","");
        }
        if (copy.contains(".")) {
            parts = copy.split("\\.");
            len = parts[1].length();
            fracpart = Long.parseLong(parts[1],12);
            fracpart *= Math.pow(12, -len);
            if (isneg == 1) {
                return (Long.parseLong(parts[0],12) + fracpart) * -1;
            } else {
                return Long.parseLong(parts[0],12) + fracpart;
            }
        } else {
            if (isneg == 1) {
                return Long.parseLong(copy,12) * -1;
            } else {
                return Long.parseLong(copy,12);
            }
        }
    }
    private static String dectodoz(double number)
    {
        int isneg = 0;
        String doznum = "";
        String decnum = Double.toString(number);
        int nextdig;
        String chardig;

        if (decnum.charAt(0) == '-') {
            isneg = 1;
            number *= -1;
        }
        while (number >= 12) {
            nextdig = (int)number % 12;
            number /= 12;
            chardig = getdozdig(nextdig);
            doznum = doznum + chardig;
        }
        nextdig = (int)number % 12;
        number /= 12;
        chardig = getdozdig(nextdig);
        doznum += chardig;
        if (isneg == 1) {
            doznum = doznum + '-';
        }
        doznum = reverse(doznum);
        return doznum;
    }
    public static String dozenalize(String decnumber)
    {
        String decnum;
        String[] parts;
        String wholenum;
        double fracdec;
        int len;
        String fracnum;
        int i;
        int holder;

        decnum = decnumber;
        if (decnum.contains(".")) {
            parts = decnum.split("\\.");
            wholenum = dectodoz(Double.parseDouble(parts[0]));
            len = parts[1].length();
            fracdec = Double.parseDouble("0."+parts[1]);
            fracnum = "";
            for (i = 0; i < MainActivity.precision; ++i) {
                fracdec *= 12;
                holder = (int)Math.floor(fracdec);
                fracdec -= holder;
                fracnum += getdozdig(holder);
            }
            return wholenum+";"+fracnum;
        }
        return dectodoz(Double.parseDouble(decnum));
    }
    private static String getdozdig(final int num) {
        switch (num) {
            case 0: return "0";
            case 1: return "1";
            case 2: return "2";
            case 3: return "3";
            case 4: return "4";
            case 5: return "5";
            case 6: return "6";
            case 7: return "7";
            case 8: return "8";
            case 9: return "9";
            case 10: return "a";
            case 11: return "b";
        }
        return "0";
    }
    private static String reverse (String str) {
        char[] holder = str.toCharArray();
        String result = "";
        for (int i = holder.length-1; i>=0; --i)
            result += holder[i];
        return result;
    }
}