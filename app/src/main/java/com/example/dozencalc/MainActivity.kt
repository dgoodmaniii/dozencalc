package com.example.dozencalc

import android.app.Activity
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.example.dozencalc.baseconv.dozenalize
import kotlinx.android.synthetic.main.activity_main.*
import android.widget.Spinner
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent





class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        // use position to know the selected item
        precision = position
        val toast = Toast.makeText(applicationContext, position, Toast.LENGTH_SHORT)
        toast.show()
        System.out.println("HEY")
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {
        precision = 4
    }

    var probf: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val help = findViewById<TextView>(R.id.helpfield)
        val helpscreen = Intent(this,Helpscreen::class.java)
        help.setOnClickListener {startActivity(helpscreen)}

        val no0 = findViewById<Button>(R.id._0)
        val no1 = findViewById<Button>(R.id._1)
        val no2 = findViewById<Button>(R.id._2)
        val no3 = findViewById<Button>(R.id._3)
        val no4 = findViewById<Button>(R.id._4)
        val no5 = findViewById<Button>(R.id._5)
        val no6 = findViewById<Button>(R.id._6)
        val no7 = findViewById<Button>(R.id._7)
        val no8 = findViewById<Button>(R.id._8)
        val no9 = findViewById<Button>(R.id._9)
        val noX = findViewById<Button>(R.id._X)
        val noE = findViewById<Button>(R.id._E)
        val dit = findViewById<Button>(R.id._dit)
        no0.setOnClickListener {number("0")}
        no1.setOnClickListener {number("1")}
        no2.setOnClickListener {number("2")}
        no3.setOnClickListener {number("3")}
        no4.setOnClickListener {number("4")}
        no5.setOnClickListener {number("5")}
        no6.setOnClickListener {number("6")}
        no7.setOnClickListener {number("7")}
        no8.setOnClickListener {number("8")}
        no9.setOnClickListener {number("9")}
        noX.setOnClickListener {number("X")}
        noE.setOnClickListener {number("E")}
        dit.setOnClickListener {number(";")}
        val div = findViewById<Button>(R.id._div)
        val mult = findViewById<Button>(R.id._mult)
        val subt = findViewById<Button>(R.id._subt)
        val add = findViewById<Button>(R.id._add)
        val exp = findViewById<Button>(R.id._exp)
        val sqrt = findViewById<Button>(R.id._sqrt)
        div.setOnClickListener {operator("/")}
        mult.setOnClickListener {operator("*")}
        subt.setOnClickListener {operator("-")}
        add.setOnClickListener {operator("+")}
        exp.setOnClickListener {operator("^")}
        sqrt.setOnClickListener {operator("√")}
        val eq = findViewById<Button>(R.id._eq)
        eq.setOnClickListener {docalc()}
        val clear = findViewById<Button>(R.id._clear)
        clear.setOnClickListener {clearfield()}
        val opar = findViewById<Button>(R.id._opar)
        opar.setOnClickListener {operator("(")}
        val cpar = findViewById<Button>(R.id._cpar)
        cpar.setOnClickListener {operator(")")}
        val back = findViewById<Button>(R.id._back)
        back.setOnClickListener {backup()}
        val nega = findViewById<Button>(R.id._nega)
        nega.setOnClickListener {number(" -")}
        var orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            val sin = findViewById<Button>(R.id._sin)
            sin.setOnClickListener {operator("sin")}
            val cos = findViewById<Button>(R.id._cos)
            cos.setOnClickListener {operator("cos")}
            val tan = findViewById<Button>(R.id._tan)
            tan.setOnClickListener {operator("tan")}
            val asin = findViewById<Button>(R.id._asin)
            asin.setOnClickListener {operator("asin")}
            val acos = findViewById<Button>(R.id._acos)
            acos.setOnClickListener {operator("acos")}
            val atan = findViewById<Button>(R.id._atan)
            atan.setOnClickListener {operator("atan")}
            val recip = findViewById<Button>(R.id._recip)
            recip.setOnClickListener {operator("recip")}
            val pi = findViewById<Button>(R.id._pi)
            pi.setOnClickListener {operator("3;18480949")}
            val eul = findViewById<Button>(R.id._eul)
            eul.setOnClickListener {operator("2;87523606")}
            val log = findViewById<Button>(R.id._log)
            log.setOnClickListener {operator("log")}
            val logx = findViewById<Button>(R.id._logx)
            logx.setOnClickListener {operator("logx")}
            val ln = findViewById<Button>(R.id._ln)
            ln.setOnClickListener {operator("ln")}
            val dlg = findViewById<Button>(R.id._dlg)
            dlg.setOnClickListener {operator("dlg")}
            val angleu = findViewById<Button>(R.id._angleu)
            angleu.setOnClickListener {angleunit()}
            val precspin = findViewById<Spinner>(R.id._prec)
            val listofitems = arrayOf("0","1","2","3","4","5","6","7","8","9","X","E","10")
            val spinnerArrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, listofitems)
            //selected item will look like a spinner set from XML
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            precspin.adapter = spinnerArrayAdapter
            precspin.setSelection(4)
            precspin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    precision = 4;
                }
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    precision = position;
                }
            }
        }
    }

    fun angleunit() {
        if (currau == "rad") {
            currau = "deg"
            anglefield.text = "deg"
        } else if (currau == "deg") {
            currau = "rad"
            anglefield.text = "rad"
        }
    }

    fun backup() {
        this.probf = probf.replace(Regex(" $"),"")
        if (probf.length > 0)
            this.probf = probf.substring(0,probf.length -1)
        answerfield.text = probf
    }

    fun number(a: String) {
        this.probf = probf.plus(a)
        answerfield.text = probf
    }
    fun operator(a: String) {
        this.probf = probf.plus(" ").plus(a).plus(" ")
        answerfield.text = probf
    }
    fun docalc() {
        this.probf = probf.replace('X','a')
        this.probf = probf.replace('E','b')
        this.probf = probf.replace(';','.')
        this.probf = probf.replace("√","sqrt")
        this.probf = probf.replace("acos","zcos")
        this.probf = probf.replace("asin","zsin")
        this.probf = probf.replace("atan","ztzn")
        this.probf = probf.replace("tan","tzn")
        val ans = meval.eval(probf)
        this.probf = dozenalize(ans.toString())
        this.probf = probf.replace('a','X')
        this.probf = probf.replace('b','E')
        this.probf = probf.replace('.',';')
        answerfield.text = probf
    }
    fun clearfield() {
        this.probf = ""
        answerfield.text = probf
    }

    companion object {
        @kotlin.jvm.JvmField
        var currau: String = "rad"
        @kotlin.jvm.JvmField
        var precision: Int = 4
    }
}
/*
class SpinnerActivity : Activity(), AdapterView.OnItemSelectedListener {
    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
    }
    override fun onNothingSelected(parent: AdapterView<*>) {
        // Another interface callback
    }
}*/