package com.example.dozencalc;

// from https://stackoverflow.com/questions/3422673/how-to-evaluate-a-math-expression-given-in-string-form, released into public domain
//System.out.println(eval("((4 - 2^3 + 1) * -sqrt(3*3+4*4)) / 2"));

import static com.example.dozencalc.baseconv.decimalize;
import static java.lang.Math.*;

public class meval {
    public static double eval(final String str) {
        return new Object() {
            int pos = -1, ch;

            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ') nextChar();
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char)ch);
                return x;
            }

            // Grammar:
            // expression = term | expression `+` term | expression `-` term
            // term = factor | term `*` factor | term `/` factor
            // factor = `+` factor | `-` factor | `(` expression `)`
            //        | number | functionName factor | factor `^` factor

            double parseExpression() {
                double x = parseTerm();
                for (;;) {
                    if      (eat('+')) x += parseTerm(); // addition
                    else if (eat('-')) x -= parseTerm(); // subtraction
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (;;) {
                    if      (eat('*')) x *= parseFactor(); // multiplication
                    else if (eat('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            double parseFactor() {
                if (eat('+')) return parseFactor(); // unary plus
                if (eat('-')) return -parseFactor(); // unary minus

                double x;
                int startPos = this.pos;
                if (eat('(')) { // parentheses
                    x = parseExpression();
                    eat(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.' || ch == 'a' || ch == 'b') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.' || ch == 'a' || ch == 'b') nextChar();
                    //x = Double.parseDouble(str.substring(startPos, this.pos));
                    x = decimalize(str.substring(startPos, this.pos));
                } else if (ch >= 'c' && ch <= 'z') { // functions
                    while (ch >= 'c' && ch <= 'z') nextChar();
                    String func = str.substring(startPos, this.pos);
                    x = parseFactor();
                    double y;
                    if (MainActivity.currau.equals("deg")) {
                        y = toRadians(x);
                    } else {
                        y = x;
                    }
                    System.out.println("HERE:  "+func+"  "+str+"  |"+x+"|");
                    if (func.equals("sqrt")) x = sqrt(x);
                    else if (func.equals("sin")) x = sin(y);
                    else if (func.equals("cos")) x = cos(y);
                    else if (func.equals("tzn")) x = tan(y);
                    else if (func.equals("zsin")) x = (y != x) ? toDegrees(asin(x)) : asin(x);
                    else if (func.equals("zcos")) x = (y != x) ? toDegrees(acos(x)) : acos(x);
                    else if (func.equals("ztzn")) x = (y != x) ? toDegrees(atan(x)) : atan(x);
                    else if (func.equals("recip")) x = 1 / x;
                    else if (func.equals("ln")) x = log(x);
                    else if (func.equals("logx")) x = log10(x);
                    else if (func.equals("log")) x = log10(x) / log10(12);
                    else if (func.equals("dlg")) x = log10(x) / log10(2);
                    else throw new RuntimeException("Unknown function: " + func);
                } else {
                    throw new RuntimeException("Unexpected: " + (char)ch);
                }

                if (eat('^')) x = pow(x, parseFactor()); // exponentiation

                return x;
            }
        }.parse();
    }
}